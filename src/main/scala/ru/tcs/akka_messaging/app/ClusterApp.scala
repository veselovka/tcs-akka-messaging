package ru.tcs.akka_messaging.app

import akka.actor.ActorSystem
import ru.tcs.akka_messaging.actor.MasterActor.Stop
import ru.tcs.akka_messaging.actor.UIActor
import ru.tcs.akka_messaging.app.Settings._
import ru.tcs.akka_messaging.ui.model.{Node, PerformanceStat}
import ru.tcs.akka_messaging.ui.view.UI

import scala.concurrent.Await
import scala.concurrent.duration._
import scalafx.application.JFXApp
import scalafx.collections.ObservableBuffer
import scala.concurrent.ExecutionContext.Implicits.global

object ClusterApp extends JFXApp {

  val system = ActorSystem(actorSystemName, config.withRoles(Seq(masterRole)))
  val nodes = ObservableBuffer.empty[Node]
  val perfStats = ObservableBuffer.empty[PerformanceStat]
  val uiActor = system.actorOf(UIActor.props(nodes, perfStats, uIInterval), "ui-actor")

  stage = UI.mkStage(uiActor, nodes, perfStats)

  override def stopApp(): Unit = {
    uiActor ! Stop
    Await.ready(system.terminate(), 5.seconds).onComplete {
      case _ ⇒ sys.exit(0)
    }
  }
}

package ru.tcs.akka_messaging.app

import java.util.concurrent.TimeUnit

import akka.actor.AddressFromURIString
import com.typesafe.config.{Config, ConfigFactory}

import scala.collection.JavaConversions.asScalaBuffer
import scala.concurrent.duration.Duration

object Settings {
  val config = ConfigFactory.load()
  val appConfig = config.getConfig("app")

  val actorSystemName = appConfig.getString("system")
  val nodeRole = appConfig.getString("node")
  val masterRole = appConfig.getString("master")

  val nodeConfig = ConfigFactory
    .parseString("akka.remote.netty.tcp.port = 0")
    .withFallback(config.withRoles(Seq(nodeRole)))

  val msgInterval: Duration = appConfig.getDuration("msg-interval").toCoarsest
  val statInterval: Duration = appConfig.getDuration("stat-interval").toCoarsest
  val uIInterval: Duration = appConfig.getDuration("ui-refresh-interval").toCoarsest

  val seedNodes = config.getStringList("akka.cluster.seed-nodes").toList.map(AddressFromURIString.parse)
  val schedulerTick: Duration = config.getDuration("akka.scheduler.tick-duration")

  implicit class ConfigExtensions(val config: Config) extends AnyVal {
    def withRoles(roles: Seq[String]) =
      ConfigFactory.parseString(s"akka.cluster.roles = ${roles.mkString("[", ",", "]")}").withFallback(config)
  }

  implicit def durationConversion(duration: java.time.Duration): Duration =
    Duration(duration.toMillis, TimeUnit.MILLISECONDS)
}

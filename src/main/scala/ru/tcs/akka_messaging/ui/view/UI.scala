package ru.tcs.akka_messaging.ui.view

import java.lang.Character.isDigit
import java.util.concurrent.TimeUnit

import akka.actor.ActorRef
import ru.tcs.akka_messaging.actor.MasterActor._
import ru.tcs.akka_messaging.actor.UIActor.{Buttons, UIInterval}
import ru.tcs.akka_messaging.app.Settings._
import ru.tcs.akka_messaging.ui.model.{PerformanceStat, Node}

import scala.concurrent.duration._
import scala.util.Try
import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.collections.ObservableBuffer
import scalafx.event.ActionEvent
import scalafx.geometry.{HPos, Insets, Pos}
import scalafx.scene.control.TableColumn._
import scalafx.scene.control._
import scalafx.scene.layout.{GridPane, HBox, Pane}
import scalafx.scene.text.Text
import scalafx.scene.Scene
import javafx.scene.control.{Button ⇒ jfxButton}
import javafx.scene.layout.{Pane ⇒ jfxPane}

object UI {
  def mkStage(uiActor: ActorRef, nodes: ObservableBuffer[Node], perfStats: ObservableBuffer[PerformanceStat]) = {
    new JFXApp.PrimaryStage {
      title.value = "Akka Messaging"
      width = 1090
      height = 720
      scene = new Scene { content = clusterView(uiActor, nodes, perfStats) }
    }
  }

  private def clusterView(uiActor: ActorRef, nodes: ObservableBuffer[Node], perfStats: ObservableBuffer[PerformanceStat]) = {
    val clusterPane = new GridPane() {
      maxWidth = 600
      hgap = 20
      vgap = 20
      padding = Insets(20)
    }
    initClusterInfoView(clusterPane, uiActor, nodes)
    initClusterSettingsView(clusterPane, uiActor)

    val performancePane = new GridPane() {
      maxWidth = 700
      hgap = 20
      vgap = 20
      padding = Insets(20)
    }
    initPerformanceStatView(performancePane, perfStats)
    initPerformanceSettingsView(performancePane, buttonsOn(clusterPane), perfStats, uiActor)

    new HBox {
      alignment = Pos.BaselineRight
      children = List(clusterPane, performancePane)
    }
  }

  private def buttonsOn(pane: Pane): Seq[jfxButton] = {
    pane.children.collect {
      case b: jfxButton ⇒
        Seq(b)
      case p: jfxPane ⇒
        buttonsOn(new Pane(p))
    }.flatten
  }

  private def initClusterInfoView(gridPane: GridPane, uiActor: ActorRef, nodes: ObservableBuffer[Node]) = {
    val nodeView = new TableView(nodes) {
      columns ++= List(
        new TableColumn[Node, String] {
          text = "Адрес"
          cellValueFactory = { _.value.address }
          minWidth = 300
          alignmentInParent = Pos.BaselineLeft
        },
        new TableColumn[Node, Int]() {
          text = "Сообщений/сек"
          cellValueFactory = { _.value.msgPerSec }
          prefWidth = 189
          alignmentInParent = Pos.BaselineRight
        }
      )
    }

    val addNodeBtn = new Button {
      text = "Добавить узел"
      prefWidth = 150
      onAction = { _: ActionEvent ⇒ uiActor ! AddNode }
    }
    val rmNodeBtn = new Button {
      text = "Удалить узел"
      prefWidth = 150
      onAction = { _: ActionEvent ⇒ uiActor ! RemoveNode }
    }
    val btnBox = new HBox {
      padding = Insets(0, 0, 20, 0)
      spacing = 10
      alignment = Pos.BaselineRight
      children = List(addNodeBtn, rmNodeBtn)
    }

    gridPane.add(nodeView, 0, 0, 2, 1)
    gridPane.add(btnBox, 0, 1, 2, 1)
  }

  private def initClusterSettingsView(gridPane: GridPane, uiActor: ActorRef) = {
    val msgIntervalLabel = new Text { text = "Интервал отправки сообщений [мс]" }
    val msgIntervalText = new TextField {
      text = msgInterval.toMillis.toString
      alignment = Pos.BaselineRight
    }
    val statIntervalLabel = new Text { text = "Интервал сбора статистики [мс]" }
    val statIntervalText = new TextField {
      text = statInterval.toMillis.toString
      alignment = Pos.BaselineRight
    }
    val uiIntervalLabel = new Text { text = "Частота обновления интерфейса [мс]" }
    val uiIntervalText = new TextField {
      text = uIInterval.toMillis.toString
      alignment = Pos.BaselineRight
    }
    val saveSettingsBtn = new Button {
      text = "Сохранить настройки"
      prefWidth = 200
      onAction = { _: ActionEvent ⇒
        val msgInterval = msgIntervalText.getText
        if (msgInterval.isNum) uiActor ! MsgInterval(msgInterval)
        val statInterval = statIntervalText.getText
        if (statInterval.isNum) uiActor ! StatInterval(statInterval)
        val uiInterval = uiIntervalText.getText
        if (uiInterval.isNum) uiActor ! UIInterval(uiInterval)
      }
    }

    gridPane.add(msgIntervalLabel, 0, 2)
    gridPane.add(msgIntervalText, 1, 2)
    gridPane.add(statIntervalLabel, 0, 3)
    gridPane.add(statIntervalText, 1, 3)
    gridPane.add(uiIntervalLabel, 0, 4)
    gridPane.add(uiIntervalText, 1, 4)
    gridPane.add(saveSettingsBtn, 1, 5)
    GridPane.setHalignment(saveSettingsBtn, HPos.Right)
  }

  private def initPerformanceStatView(gridPane: GridPane, perfStats: ObservableBuffer[PerformanceStat]) = {
    val perfTable = new TableView(perfStats) {
      columns ++= List(
        new TableColumn[PerformanceStat, Int] {
          text = "Количество узлов"
          cellValueFactory = { _.value.nodes }
          prefWidth = 150
          alignmentInParent = Pos.BaselineLeft
        },
        new TableColumn[PerformanceStat, String] {
          text = "Интервал [мс]"
          cellValueFactory = { _.value.msgInterval }
          prefWidth = 150
          alignmentInParent = Pos.BaselineLeft
        },
        new TableColumn[PerformanceStat, Int]() {
          text = "Сообщений/сек"
          cellValueFactory = { _.value.msgPerSec }
          prefWidth = 206
          alignmentInParent = Pos.BaselineRight
        }
      )
    }

    gridPane.add(perfTable, 0, 0, 3, 1)
  }

  private def initPerformanceSettingsView(gridPane: GridPane, controlsToBlock: Iterable[jfxButton],
                                          perfStats: ObservableBuffer[PerformanceStat], uiActor: ActorRef) = {

    val nodeRangeLabel = new Text { text = "Количество узлов" }
    val nodeRangeFromText = new TextField {
      text = "2"
      alignment = Pos.BaselineRight
      prefWidth = 110
    }
    val nodeRangeToText = new TextField {
      text = "4"
      alignment = Pos.BaselineRight
      prefWidth = 110
    }

    val msgTsRangeLabel = new Text { text = "Интервал отправки сообщений [мс]" }
    val msgTsRangeFromText = new TextField {
      text = "20"
      alignment = Pos.BaselineRight
      prefWidth = 110
    }
    val msgTsRangeToText = new TextField {
      text = "40"
      alignment = Pos.BaselineRight
      prefWidth = 110
    }
    val msgTsStepLabel = new Text { text = "Шаг интервала [мс]" }
    val msgTsStepText = new TextField {
      text = "20"
      alignment = Pos.BaselineRight
      prefWidth = 110
    }

    val estimatePerfBtn: Button = new Button {
      text = "Оценить производительность"
      prefWidth = 240
    }
    estimatePerfBtn.onAction = { _: ActionEvent ⇒
      for {
        nodeFrom ← Try(nodeRangeFromText.getText.toInt)
        nodeTo ← Try(nodeRangeToText.getText.toInt)
        msgTsFrom ← Try(msgTsRangeFromText.getText.toInt)
        msgTsTo ← Try(msgTsRangeToText.getText.toInt)
        msgStep ← Try(msgTsStepText.getText.toInt)
      } {
        perfStats.clear()
        val nodeRange = Range(nodeFrom, nodeTo, 1)
        val msgTsRange = Range(msgTsFrom, msgTsTo, msgStep)
        controlsToBlock.foreach(_.disable_=(true))
        estimatePerfBtn.disable_=(true)
        uiActor ! EstimatePerformance(nodeRange, msgTsRange)
      }
    }

    gridPane.add(nodeRangeLabel, 0, 1)
    gridPane.add(nodeRangeFromText, 1, 1)
    gridPane.add(nodeRangeToText, 2, 1)
    gridPane.add(msgTsRangeLabel, 0, 2)
    gridPane.add(msgTsRangeFromText, 1, 2)
    gridPane.add(msgTsRangeToText, 2, 2)
    gridPane.add(msgTsStepLabel, 0, 3)
    gridPane.add(msgTsStepText, 2, 3)
    gridPane.add(estimatePerfBtn, 1, 4, 2, 1)
    GridPane.setHalignment(estimatePerfBtn, HPos.Right)

    uiActor ! Buttons(controlsToBlock.toSeq :+ estimatePerfBtn.delegate)
  }

  implicit class StringExtensions(val str: String) extends AnyVal {
    def isNum = str.forall(isDigit)
  }

  implicit def string2Duration(str: String): Duration =
    Duration(str.toLong, TimeUnit.MILLISECONDS).toCoarsest
}

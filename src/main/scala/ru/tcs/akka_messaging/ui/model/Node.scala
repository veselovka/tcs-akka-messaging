package ru.tcs.akka_messaging.ui.model

import scalafx.beans.property.{ObjectProperty, StringProperty}

class Node(_address: String, _msgPerSecond: Int = 0) {
  val address = new StringProperty(this, "Адрес", _address)
  val msgPerSec = new ObjectProperty(this, "Сообщений/сек", _msgPerSecond)
}

package ru.tcs.akka_messaging.ui.model

import scala.concurrent.duration.Duration
import scalafx.beans.property.{ObjectProperty, StringProperty}

class PerformanceStat(_nodes: Int, _msgInterval: Duration, _msgPerSecond: Int) {
  val nodes = new ObjectProperty(this, "Количество узлов", _nodes)
  val msgInterval = new StringProperty(this, "Интервал отправки сообщений", _msgInterval.toMillis.toString)
  val msgPerSec = new ObjectProperty(this, "Сообщений/сек", _msgPerSecond)
}

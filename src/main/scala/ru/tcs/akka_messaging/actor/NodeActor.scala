package ru.tcs.akka_messaging.actor

import akka.actor._
import akka.cluster.Cluster
import akka.cluster.pubsub.DistributedPubSub
import akka.cluster.pubsub.DistributedPubSubMediator.{Publish, Subscribe}
import ru.tcs.akka_messaging.actor.MasterActor.{MsgInterval, StatInterval}
import ru.tcs.akka_messaging.actor.UIActor.NodeStat

import scala.concurrent.duration._

class NodeActor(id: String, schedulerInterval: Duration, msgInterval: Duration,
                statInterval: Duration, addresses: collection.immutable.Seq[Address], statActor: ActorRef)
  extends Actor with ActorLogging
{
  import NodeActor._

  // т.к. сообщения необходимо отправлять не реже, чем раз в msgInterval,
  // возьмем запас = scheduler-tick-duration + 5ms
  val delta = schedulerInterval + 5.millis
  require(isValidMsgInterval(msgInterval), s"MsgInterval must be greater than $delta")

  var msgDurUpperBound = msgInterval
  var msgDur = msgDurUpperBound - delta
  var statDur = statInterval
  var statDurNanos = statDur.toNanos

  val cluster = Cluster(context.system)
  cluster.joinSeedNodes(addresses)
  val address = cluster.selfAddress

  val mediator = DistributedPubSub(context.system).mediator
  mediator ! Subscribe(Topic, self)

  val msg = Message(id)
  var msgTimestamps = List.empty[Long]

  context.system.scheduler.scheduleOnce(msgDur, self, Tick)

  override def receive: Receive = messaging orElse settingsUpdate

  def settingsUpdate: Receive = {
    case MsgInterval(msgInterv) if msgInterv != msgDurUpperBound && isValidMsgInterval(msgInterval) ⇒
      log.info(s"[Node-$id] update messaging interval from $msgDurUpperBound to $msgInterv")
      msgDurUpperBound = msgInterv
      msgDur = msgDurUpperBound - delta
    case StatInterval(statInterv) if statInterv != statDur ⇒
      log.info(s"[Node-$id] update statistics interval from $statDur to $statInterv")
      statDur = statInterv
      statDurNanos = statDur.toNanos
  }

  def messaging: Receive = {
    case Message(payload) if payload != id ⇒
      log.debug(s"[Node-$id] received message with payload='$payload'")
      val ts = System.nanoTime()
      msgTimestamps = ts :: msgTimestamps.takeWhile(t ⇒ ts - t <= statDurNanos)

      // в текущей реализации количество сообщений, обработанных за единицу времени,
      // будет некорректно первое время после увеличения statInterval
      statActor ! NodeStat(address.toString, msgTimestamps.size, msgDurUpperBound, statDur)
      log.debug(s"[Node-$id] processed ${msgTimestamps.size} messages in the last $statDur")
    case Tick ⇒
      mediator ! Publish(Topic, msg) // здесь лучше использовать сообщение SendAll, но у меня оно почему-то не заработало
      context.system.scheduler.scheduleOnce(msgDur, self, Tick)
  }

  def isValidMsgInterval(msgInterval: Duration) = delta < msgInterval
}

object NodeActor {

  val Topic = "node-messaging"

  case class Message(payload: String)
  case object Tick

  def props(id: Long, schedulerTick: Duration, msgInterval: Duration, statInterval: Duration, addresses: Seq[Address], statActor: ActorRef) =
    Props(classOf[NodeActor], id.toString, schedulerTick, msgInterval, statInterval, addresses, statActor)
}

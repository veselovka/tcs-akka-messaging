package ru.tcs.akka_messaging.actor

import java.util.concurrent.TimeUnit

import akka.actor._
import akka.cluster.Cluster
import akka.cluster.ClusterEvent._
import akka.pattern.ask
import ru.tcs.akka_messaging.actor.UIActor.{ToNormalMode, Second, NodeDown, NodeUp}
import akka.util.Timeout
import ru.tcs.akka_messaging.actor.StatActor.{StopCollecting, StartCollecting}
import ru.tcs.akka_messaging.app.Settings

import scala.collection.mutable.{Set ⇒ mSet, Map ⇒ mMap}
import scala.concurrent.duration._
import scala.util.control.NonFatal
import scala.util.{Failure, Success}


class MasterActor(uIActor: ActorRef, statActor: ActorRef) extends Actor with ActorLogging {
  import MasterActor._

  val cluster = Cluster(context.system)
  var msgInterval = Settings.msgInterval
  var statInterval = Settings.statInterval
  val nodes = mMap.empty[Address, ActorSelection]
  val unReachableNodes = mSet.empty[Address]
  val removingNodes = mSet.empty[Address]
  
  var nodeRange: Range = _
  var msgIntervalFrom: Duration = _
  var msgIntervalTo: Duration = _
  var msgIntervalStep: Duration = _

  var stopping = false

  override def preStart(): Unit = {
    cluster.subscribe(self, initialStateMode = InitialStateAsEvents,
      classOf[MemberEvent], classOf[UnreachableMember])
  }

  override def postStop(): Unit = cluster.unsubscribe(self)

  self ! AddNode

  def normalMode = normalModeClusterRequests orElse clusterEvents
  
  override def receive = normalMode

  def nodeRequests: Receive = {
    case AddNode ⇒
      if (unReachableNodes.isEmpty) {
        log.info("[Cluster] Adding new node to the cluster")
        val system = ActorSystem(Settings.actorSystemName, Settings.nodeConfig)
        system.actorOf(NodeActor.props(
          nodeId(), Settings.schedulerTick, msgInterval, statInterval, Settings.seedNodes, statActor), Settings.nodeRole
        )
      } else {
        log.warning(s"[Cluster] Failed to add node to the cluster: unreachable nodes=$unReachableNodes")
      }
    case RemoveNode ⇒
      nodes.keySet.diff(unReachableNodes).diff(removingNodes).lastOption.foreach { address ⇒
        log.info(s"[Cluster] Removing the node with address = $address")
        cluster.down(address)
        removingNodes += address
      }
    case Stop ⇒
      if (nodes.isEmpty) {
        stop()
      } else {
        stopping = true
        nodes.keys.foreach(cluster.down)
      }
  }

  def normalModeClusterRequests: Receive = nodeRequests orElse {
    case msg: MsgInterval ⇒
      setMsgInterval(msg)
    case msg: StatInterval ⇒
      setStatInterval(msg)

    case EstimatePerformance(nodeCountRange, msgIntervalRange) ⇒
      nodeRange = nodeCountRange
      msgIntervalFrom = Duration(msgIntervalRange.start, TimeUnit.MILLISECONDS)
      msgIntervalTo = Duration(msgIntervalRange.end, TimeUnit.MILLISECONDS)
      msgIntervalStep = Duration(msgIntervalRange.step, TimeUnit.MILLISECONDS)
      context.become(perfEstimationMode(nodeCount = nodeRange.start))
      self ! StartEstimation
  }

  def clusterEvents: Receive = {
    case MemberUp(member) ⇒
      log.info("[Listener] Member is up: {}", member.address)
      if (member.hasRole(Settings.nodeRole)) {
        val actor = nodeActor(member.address)
        nodes += member.address -> actor
        uIActor ! NodeUp(member.address.toString)
        pushSettings(actor) // на тот случай, если настройки изменились пока нода поднималась
      }
    case MemberRemoved(member, previousStatus) ⇒
      log.info("[Listener] Member is Removed: {} after {}", member.address, previousStatus)
      if (member.hasRole(Settings.nodeRole)) {
        nodes -= member.address
        unReachableNodes -= member.address
        removingNodes -= member.address
        uIActor ! NodeDown(member.address.toString)
      }
      if (stopping && nodes.isEmpty) {
        stop()
      }
    case ReachableMember(member) ⇒
      log.info("[Listener] Member detected as reachable: {}", member)
      if (member.hasRole(Settings.nodeRole)) {
        val actor = nodeActor(member.address)
        unReachableNodes -= member.address
        nodes += member.address -> actor
        uIActor ! NodeUp(member.address.toString)
        pushSettings(actor) // на тот случай, если настройки изменились пока нода была недоступна
      }
    case UnreachableMember(member) ⇒
      log.info("[Listener] Member detected as unreachable: {}", member)
      if (member.hasRole(Settings.nodeRole)) {
        nodes -= member.address
        unReachableNodes += member.address
        uIActor ! NodeDown(member.address.toString)
      }
    case ev: MemberEvent ⇒
      log.debug("[Listener] Event {}", ev)
  }

  // Режим оценки производительности сделан для наглядности и реализован максимально просто, но не очень надежно.
  // В реальном приложении лучше использовать какие-нибудь метрики, либо более надежный механизм
  def perfEstimationMode(nodeCount: Int): Receive = nodeRequests orElse {
    case StartEstimation ⇒
      log.info(s"Starting performance estimation with settings: node-interval=${nodeRange.start}-${nodeRange.end}; " +
        s"msg-interval=$msgIntervalFrom-$msgIntervalTo, step=$msgIntervalStep")
      
      setMsgInterval(MsgInterval(msgIntervalFrom))
      setStatInterval(StatInterval(Second))

      // по-хорошему здесь нужно дождаться, когда unReachableNodes и removingNodes вернутся в кластер или отвалятся окончательно
      val nodeDiff = nodes.size - nodeCount
      if (nodeDiff != 0) {
        if (nodeDiff > 0) for (_ ← 1 to nodeDiff) self ! RemoveNode
        else for (_ ← 1 to (-1 * nodeDiff)) self ! AddNode
      } else {
        notifyStatActor()
      }

    case MemberUp(member) ⇒
      if (member.hasRole(Settings.nodeRole)) {
        val actor = nodeActor(member.address)
        nodes += member.address -> actor
        uIActor ! NodeUp(member.address.toString)
        if (nodes.size == nodeCount) {
          notifyStatActor()
        }
      }
    case MemberRemoved(member, previousStatus) ⇒
      if (member.hasRole(Settings.nodeRole)) {
        nodes -= member.address
        unReachableNodes -= member.address
        removingNodes -= member.address
        uIActor ! NodeDown(member.address.toString)
        if (nodes.size == nodeCount) {
          notifyStatActor()
        }
      }
      if (stopping && nodes.isEmpty) {
        stop()
      }

    case Tick ⇒
      if (msgInterval + msgIntervalStep <= msgIntervalTo) {
        setMsgInterval(MsgInterval(msgInterval + msgIntervalStep))
        notifyStatActor()
      } else if (nodeCount + 1 <= nodeRange.end) {
        setMsgInterval(MsgInterval(msgIntervalFrom))
        context.become(perfEstimationMode(nodeCount = nodeCount + 1))
        self ! AddNode
      } else {
        stopEstimation()
      }
  }

  def broadcast(msg: Any) = nodes.values.foreach(_ ! msg)

  def nodeActor(address: Address) =
    context.actorSelection(RootActorPath(address) / "user" / Settings.nodeRole)

  def notifyStatActor() = {
    (statActor ? StartCollecting(msgInterval)).onComplete {
      case Success(_) ⇒
        context.system.scheduler.scheduleOnce(PerformanceDelay, self, Tick)
      case Failure(ex) ⇒
        log.error(ex, "Error while interacting with StatActor")
        stopEstimation()
    }
  }
  
  def stopEstimation() = {
    log.info("Stopping performance estimation")
    uIActor ! ToNormalMode
    context.become(normalMode)
    (statActor ? StopCollecting).onFailure {
      case NonFatal(ex) ⇒
        log.error(ex, "Error while stopping StatActor")
    }
  }

  def pushSettings(actor: ActorSelection) = {
    actor ! MsgInterval(msgInterval)
    actor ! StatInterval(statInterval)
  }

  def setMsgInterval(msg: MsgInterval) = {
    if (msgInterval != msg.value) {
      log.info(s"[Cluster] Changing messaging interval from $msgInterval to ${msg.value}")
      msgInterval = msg.value
      broadcast(msg)
    }
  }

  def setStatInterval(msg: StatInterval) = {
    if (statInterval != msg.value) {
      log.info(s"[Cluster] Changing statistics interval from $statInterval to ${msg.value}")
      statInterval = msg.value
      broadcast(msg)
    }
  }

  def stop() = {
    cluster.down(cluster.selfAddress)
    context.system.terminate()
  }
}

object MasterActor {

  private var id: Long = 0
  private val PerformanceDelay = 10.seconds
  implicit val timeout: Timeout = 3.seconds

  sealed trait ClusterCmd

  case object AddNode extends ClusterCmd
  case object RemoveNode extends ClusterCmd

  case class MsgInterval(value: Duration) extends ClusterCmd
  case class StatInterval(value: Duration) extends ClusterCmd

  case class EstimatePerformance(nodeRange: Range, msgTsRange: Range) extends ClusterCmd
  private case object StartEstimation

  case object Stop extends ClusterCmd

  case object Tick
  
  def nodeId() = {
    id = id + 1
    id
  }

  def props(uIActor: ActorRef, statActor: ActorRef) =
    Props(classOf[MasterActor], uIActor, statActor)
}

package ru.tcs.akka_messaging

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.{Duration, FiniteDuration}

package object actor {

  implicit val executionContext = global

  implicit def duration2FiniteDuration(duration: Duration): FiniteDuration =
    FiniteDuration(duration._1, duration._2)
}

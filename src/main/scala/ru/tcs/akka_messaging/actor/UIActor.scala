package ru.tcs.akka_messaging.actor

import java.util.concurrent.TimeUnit
import javafx.scene.control.{Button ⇒ jfxButton}

import akka.actor.{Actor, ActorLogging, Props}
import ru.tcs.akka_messaging.actor.MasterActor.ClusterCmd
import ru.tcs.akka_messaging.ui.model.{PerformanceStat, Node}

import scala.collection.mutable.{Buffer ⇒ mBuffer, Map ⇒ mMap}
import scala.concurrent.duration.Duration
import scalafx.collections.ObservableBuffer

class UIActor(nodes: ObservableBuffer[Node], perfStats: ObservableBuffer[PerformanceStat], uiInterval: Duration)
  extends Actor with ActorLogging 
{
  import UIActor._

  var uiRefreshInterval = uiInterval
  val statActor = context.actorOf(StatActor.props(self, uiInterval), "stat-actor")
  val masterActor = context.actorOf(MasterActor.props(self, statActor), "master-actor")
  val nodeStatByAddress = mMap.empty[String, Seq[Double]].withDefaultValue(Seq.empty)
  var buttons: Iterable[jfxButton] = _

  context.system.scheduler.scheduleOnce(uiRefreshInterval, self, Tick)

  override def receive: Receive = uiInfo orElse clusterCommands orElse settingsChange

  def uiInfo: Receive = {
    case NodeUp(address) ⇒
      log.debug(s"[UI] add node $address")
      nodes += new Node(address)
    case NodeDown(address) ⇒
      log.debug(s"[UI] remove node $address")
      nodes.removeFirst(_.address.value != address)
    case NodeStat(address, msg, _, statInterval) ⇒
      log.debug(s"[UI] receive node statistics: address=$address; msg=$msg; interval=$statInterval")
      nodeStatByAddress += address → (nodeStatByAddress(address) :+ msg * Second / statInterval)
    case Tick ⇒
      nodes.foreach { node ⇒
        val stat = nodeStatByAddress(node.address.value)
        val average = if (stat.nonEmpty) (stat.sum / stat.size).toInt else 0
        node.msgPerSec.value_=(average)
      }
      nodeStatByAddress.clear()
      context.system.scheduler.scheduleOnce(uiRefreshInterval, self, Tick)
    case PerformanceStatistics(nodesCount, interval, msg) ⇒
      perfStats += new PerformanceStat(nodesCount, interval, msg)
    case ToNormalMode ⇒
      buttons.foreach(_.setDisable(false))
  }

  def clusterCommands: Receive = {
    case msg: ClusterCmd ⇒
      masterActor forward msg
  }
  
  def settingsChange: Receive = {
    case msg@UIInterval(value) ⇒
      uiRefreshInterval = value
      statActor forward msg
    case Buttons(btns) ⇒
      buttons = btns
  }
}

object UIActor {

  val Second = Duration(1, TimeUnit.SECONDS)

  case class NodeUp(address: String)
  case class NodeDown(address: String)
  case class NodeStat(address: String, msg: Int, msgInterval: Duration, statInterval: Duration)
  case class PerformanceStatistics(nodes: Int, interval: Duration, msg: Int)
  case object ToNormalMode

  case class UIInterval(value: Duration)
  case class Buttons(buttons: Iterable[jfxButton])

  case object Tick

  def props(nodes: ObservableBuffer[Node], perfStats: ObservableBuffer[PerformanceStat], uiInterval: Duration) =
    Props(classOf[UIActor], nodes, perfStats, uiInterval)

  implicit class BufferExtension[T](val buf: mBuffer[T]) extends AnyVal {
    def removeFirst(pred: T ⇒ Boolean) = {
      val (_, after) = buf.span(pred)
      after.headOption.foreach(buf.-=)
      buf
    }
  }
}

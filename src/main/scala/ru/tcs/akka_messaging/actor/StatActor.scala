package ru.tcs.akka_messaging.actor

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import ru.tcs.akka_messaging.actor.StatActor.{Ack, StartCollecting, StopCollecting}
import ru.tcs.akka_messaging.actor.UIActor.{PerformanceStatistics, NodeStat}

import scala.concurrent.duration.Duration
import scala.collection.mutable.{Buffer ⇒ mBuffer, Set ⇒ mSet}

class StatActor(uiActor: ActorRef, uiInterval: Duration) extends Actor with ActorLogging {
  
  val stat = mBuffer.empty[Int]
  val addresses = mSet.empty[String]
  var collectingInterval: Duration = _
  
  override def receive: Receive = forwardMode
  
  def forwardMode: Receive = {
    case msg: NodeStat ⇒
      uiActor forward msg
    case StartCollecting(interval) ⇒
      collectingInterval = interval
      log.info(s"[Stat] Collecting statistics for messaging interval=$collectingInterval")
      context.become(statCollecting)
      sender() ! Ack
  }
  
  def statCollecting: Receive = {
    case StartCollecting(interval) ⇒
      publishCollected()
      collectingInterval = interval
      sender() ! Ack
      log.info(s"[Stat] Collecting statistics for messaging interval=$collectingInterval")
    case StopCollecting ⇒
      publishCollected()
      context.become(forwardMode)
      sender() ! Ack
      log.info("[Stat] Stop collecting statistics")
    case s@NodeStat(address, msg, msgInterval, _) if msgInterval == collectingInterval ⇒
      stat += msg
      addresses += address
  }

  def publishCollected() = {
    // В качестве упрощения считаем среднее кол-во обработанных сообщений.
    // Длятого, чтобы корректно оценить производительность распределенной системы нужно считать перцентили.
    val average = if (stat.nonEmpty) stat.sum / stat.size else 0
    uiActor ! PerformanceStatistics(addresses.size, collectingInterval, average)
    stat.clear()
    addresses.clear()
  }
}

object StatActor {
  case class StartCollecting(interval: Duration)
  case object StopCollecting
  case object Ack

  def props(uiActor: ActorRef, uiInterval: Duration) =
    Props(classOf[StatActor], uiActor, uiInterval)
}

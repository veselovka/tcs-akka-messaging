# Task Akka messaging

There are distributed over the network application nodes that communicate with each other by messages. Each node sends
the message, by default, not less than once every 100 MS (Ts). Each message should be, sooner or later, sent
each of the neighboring nodes. The sooner the better. There is no requirement of mandatory delivery. The number of nodes 
may vary dynamically, in the process. After receiving the message, each node should print to the console or log 
information about how much, approximately, messages were processed in the last second.

You need to emulate a cluster, distributed over the network nodes of the application on the same computer and 
develop a tool that will allow:
* add and remove node applications from a set of working nodes;
* assess the number of messages processed per second by each node;
* to change the sending interval on all nodes. Ie not for each node separately, but all at once;
* to evaluate the limit system performance. The limit can be considered as parameters of the system in which the 
addition of new nodes or a reduction in the interval of sending messages does not increase messages processed per second.

The solution must be implemented using akka framework.

Additional question, if the applicant will solve the problem. How is it possible to modify the structure of the cluster 
nodes, if we introduce a hard limit on the number of open connections on each node.


# Answers

Q: To estimate the performance limit of the system.

A: Maximum performance can be achieved by loading threads, which serve actors-handlers to the maximum, 
so that there was no downtime. Thus it is desirable to minimize the context switching of threads and organize 
storage of data so that they were always in the l1 cache, and the number of memory accesses to a minimum.
The best way to accomplish this scheme, in which each actor-handler corresponds to a separate thread. For example, we 
have 4 cores that we can use, then on the node you want to run 4 actor. Thus it is necessary not to forget to allocate 
resources for akka system needs (logging, scheduling etc). Preferably to divide these two execution contexts, 
global - for akka, and a separate context for our node-actors. In the current implementation, for simplicity there is 
only one global context for all actors. In akka in order to make a scheme actor=thread, you can use either 
PinnedDispatcher, or ThreadPool with number of threads equal to the number of cores available.
Example of calculation:
N - number of nodes
Ts - an interval of sending messages
MsgTs - time processing of a single message
CPU - number of available cores = number of actors on the node
1sec/MsgTs*CPU - maximum number of messages that is capable of processing one node
In the end, the overall performance can be calculated as: 
1sec/MsgTs*CPU = (N-1)*(1sec/Ts) or (N-1)/Ts = CPU/MsgTs
While Ts may not be less than the time interval akka scheduler.


Q: How is it possible to modify the structure of the cluster nodes, if we put a hard limit on the number of open 
connections on each node?

A: If we don't want each node to keep a list of other nodes to send all messages, you can use the gossip protocol. 
Much of the communication between nodes in akka-cluster is implemented using this algorithm. In my case, for mailing, 
I used the mechanism of subscriptions. In the case where we are not satisfied with ready implementations can write 
their own, most importantly in a distributed environment is to minimize data transfer over the network.


To run the project requires java 8 because the interface is made on javafx. I've tested the Linux version on java 1.8.0_51.
If on Windows/Mac will not start - ready to fix. In any case, at the root of the project is the screenshot of the interface.

Launch the application class ClusterApp.
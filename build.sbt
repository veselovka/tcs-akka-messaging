name := "tcs-akka-messaging"

version := "1.0"

scalaVersion := "2.11.7"

val akkaVersion = "2.4-SNAPSHOT"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor"         % akkaVersion,
  "com.typesafe.akka" %% "akka-cluster"       % akkaVersion,
  "com.typesafe.akka" %% "akka-cluster-tools" % akkaVersion,
  "org.scalafx"       %% "scalafx"            % "8.0.40-R8"
)

resolvers += "Akka Snapshots" at "http://repo.akka.io/snapshots/"
    
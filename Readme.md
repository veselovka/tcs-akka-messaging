# Task Akka messaging

There are distributed over the network application nodes that communicate with each other by messages. Each node sends
the message, by default, not less than once every 100 MS (Ts). Each message should be, sooner or later, sent
each of the neighboring nodes. The sooner the better. There is no requirement of mandatory delivery. The number of nodes 
can change dynamically. After receiving the message, each node should print to the console or log 
information about how much, approximately, messages were processed in the last second.

You need to emulate a cluster, distributed over the network nodes of the application on the same computer and 
develop a tool that will allow:
* add and remove node applications from a set of working nodes;
* estimate the number of messages processed per second by each node;
* to change the sending interval on all nodes. Ie not for each node separately, but all at once;
* to evaluate the limit system performance. The limit can be considered as parameters of the system in which the 
addition of new nodes or a reduction in the interval of sending messages does not increase messages processed per second.

The solution must be implemented using akka framework.

Additional question, if the applicant will solve the problem. How is it possible to modify the structure of the cluster 
nodes, if we introduce a hard limit on the number of open connections on each node.